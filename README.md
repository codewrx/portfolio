# Portfolio based on ExpressJs with some custom animation

## Live Links:
```
http://rajdas.herokuapp.com/
```
### Installation
```
npm install
npm start
Go to localhost:3000
```
### Created and maintained by:
```
Raj Das
fb.com/imrajdas
github.com/imrajdas
```
